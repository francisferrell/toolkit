"""
An environment-aware wrapper around the standard library `logging` module.

Example:

```
from toolkit.logging import getLogger
logger = getLogger(__name__)
```

When used in this way, you take advantage of the hierarchical nature of loggers in python, as discussed in the
standard library documentation. However, by importing `toolkit.logging` instead of `logging`, you can use
environment variables to configure the level of any logger, even those your application doesn't create.

When imported, this module uses the environment variables `LOG` and all those starting with `LOG_`, setting
their corresponding logger object's level to the value assigned to that environment variable. `LOG` defines
the level of the root logger. `LOG_*` define the levels of loggers with names derived thusly:

- `LOG_` prefix removed
- converted to lower case
- double underscores replaced with dot

So, for example, the environment variable `LOG_FOO__BAR_BAZ` would set the level on the logger named
`foo.bar_baz`, whether it had been created yet or not. Logger hierarchy continues to do its thing, so the
logger `foo.bar_baz.something` would be idirectly configured to the same level.

The values assigned to logger environment variables should be the names of log levels, case insensitive. For
example `"debug"` or `"INFO"`. Any environment variable whose value is not a valid log level name will be
silently ignored.

Separately from the logger configuration behavior, this module acts as a mirror of the `logging` module. That
is, it simply does `from logging import *`.
"""

import logging
__all__ = [ *logging.__all__, 'configure_levels_from_env' ]
del logging
from logging import *

from ._envconf import configure_levels_from_env
configure_levels_from_env()

