"""
Bridges environment variables to logger level configuration.
"""

import logging
import logging.config
from os import environ
from typing import Any



def configure_levels_from_env() -> None: # pylint: disable=invalid-name # parity with stdlib logging
    """
    Configure the level of each logger specified in all `LOG*` environment variables, as defined above.

    This method is called upon import of this module, there is no need to call it yourself.
    """
    levels: dict[str,int] = logging.getLevelNamesMapping()
    config: dict[str,Any] = {
        'version': 1,
        'incremental': True,
        'loggers': {},
    }

    for k, v in environ.items():
        is_root = False
        name = ''
        if k == 'LOG':
            is_root = True
        elif k.startswith( 'LOG_' ):
            name = k[4:].lower().replace( '__', '.' )
        else:
            continue

        try:
            level = levels[v.upper()]
        except KeyError:
            continue

        if is_root:
            config['root'] = { 'level': level }
        else:
            config['loggers'][name] = { 'level': level }

    logging.config.dictConfig( config )

