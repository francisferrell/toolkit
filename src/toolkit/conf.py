"""
Simple, multi-source configuration reader.

# Example

```python
from toolkit import conf
CONF = conf.get_default_reader()
CONF.set_defaults({
    'db.driver': 'sqlite3',
    'db.sqlite_path': '/data/db.sqlite3',
})
CONF.parse_env()

def main():
    if CONF.get('environment', 'dev') == 'production':
        # ...

def connect_db():
    try:
        if CONF.get('db.driver') == 'postgres':
            uri = CONF.get('db.postgres_uri')
        # ...
    except conf.Missing:
        logger.error('invalid database configuration')
```

The above example is setting default values for some config keys in 2 different ways:
1. Batch defaults when initializing the `Reader` instance, using `CONF.set_defaults()`.
1. At the call site of `CONF.get()`.

In `main()`, the call to get the configured `environment` will never raise `Missing` because it provides
a default value. Similarly, in `connect_db()`, the call to get `db.driver` will not raise.

By contrast, in `connect_db()`, the call to get `db.postgres_uri` would raise `Missing` if the environment
variable `CONF_DB__POSTGRES_URI` is not provided.

The example's configuration keys could be set with the following environment variables:
- `CONF_DB__DRIVER`
- `CONF_DB__POSTGRES_URI`
- `CONF_DB__SQLITE_PATH`
- `CONF_ENVIRONMENT`

# AWS Parameter Store Integration

If your application uses AWS, then you can use `parse_aws_parameters()` to read configuration data from the
AWS SSM Parameter Store.

Note that `toolkit` does *NOT* install `boto3`, your application must explicitly depend upon `boto3`. If you
have not installed `boto3` then you will receive `ModuleNotFoundError` when you call `parse_aws_parameters()`.

Note that `toolkit` does *NOT* support using a `Config` object when instantiating the SSM client. You will
need to have the appropriate environment variables defined to give access to AWS. See
[boto3 documentation](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html) for
details.
"""

from functools import cache
from os import environ
from typing import Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from mypy_boto3_ssm import SSMClient



class Missing( Exception ):
    """
    Raised when attempting to access a configuration key that is not defined.
    """



class Reader:
    """
    Reads from multiple configuration sources, overlaying their contents into a single collection of
    configuration data.

    Each `Reader` instance is initially empty. Use `set_defaults()`, `set()`, and the `parse_*()` methods
    on the instance to fill it with configuration data.

    Each call to `set()` or any `parse_*()` method will overwrite configuration values when the key is
    already in the `Reader` instance (last one wins). Use this ordering to define your application's
    configuration precedence.

    `Reader` acts like a container, so you can check for config key existnece using the `in` operator.
    """

    def __init__( self ) -> None:
        self._data: dict[str, str] = {}


    def __contains__( self, key: str ) -> bool:
        return key in self._data


    def get( self, key: str, default: Optional[str] = None ) -> str:
        """
        Returns the value assigned to `key` in the configuration data.

        When `key` is not defined in this instance, the behavior depends upon `default`. If you omit
        `default` then `Missing` is raised. If you provide a value for `default` then it is returned.
        """
        if key in self._data:
            return self._data[key]
        if default is None:
            raise Missing( key )
        return default


    def set( self, key: str, value: str ) -> None:
        """
        Set a single key in the configuration data.
        """
        self._data[key] = value


    def set_defaults( self, data: dict[str,str] ) -> None:
        """
        Set one or more keys in the configuration data without clobbering existing values.
        """
        for key, value in data.items():
            if key in self:
                continue
            self._data[key] = value


    def parse_env( self, prefix: str = 'CONF_' ) -> None:
        """
        Read configuration data from environment variables.

        Only environment variables whose names begin with `prefix` will be set in the configuration data.
        Environment variable names will be converted into config keys by the following modifications:
        1. `prefix` removed from the beginning
        1. changed to lowercase
        1. `__` (double underscore) replaced with `.` (dot).

        For example: `CONF_FOO` becomes `foo` and `CONF_BAR__BAZ_QUX` becomes `bar.baz_qux`.
        """
        prefix_length = len( prefix )
        for key, value in environ.items():
            if not key.startswith( prefix ):
                continue
            suffix = key[prefix_length:].lower().replace( '__', '.' )
            self._data[suffix] = value


    def parse_aws_parameters( self, prefix: str ) -> None:
        """
        Read configuration data from the AWS SSM parameter store.

        Only parmeters whose names begin with `prefix` will be set in the configuration data.
        Parameter names will be converted into config keys by the following modifications:
        1. `prefix` and the separating `/` character removed from the beginning
        1. changed to lowercase
        1. `-` (hyphen) replaced with `_` (underscore).
        1. `/` (slash) replaced with `.` (dot).

        For example, if `prefix="/App"`: `/App/Foo` becomes `foo` and `/App//Bar/baz_qux` becomes
        `bar.baz_qux`.

        `prefix` may be `/` if you wish to read the entire parameter hierarchy in the configured region.

        Raises `ModuleNotFoundError` if `boto3` is not installed.
        """
        if not prefix.endswith( '/' ):
            prefix = f'{prefix}/'

        paginator = _get_ssm_client().get_paginator( 'get_parameters_by_path' )
        results = paginator.paginate(
            Path = prefix,
            Recursive = True,
            WithDecryption = True
        )

        prefix_length = len( prefix )
        for page in results:
            for parameter in page['Parameters']:
                key = parameter['Name']
                value = parameter['Value']
                suffix = key[prefix_length:].lower().replace( '-', '_' ).replace( '/', '.' )
                self._data[suffix] = value



@cache
def get_default_reader() -> Reader:
    """
    Get the default `Reader` instance for this process. This will be the same object in every scope that
    it is called from.
    """
    return Reader()



@cache
def _get_ssm_client() -> 'SSMClient':
    import boto3 # pylint: disable=import-outside-toplevel # deferred import because the feature is optional
    return boto3.client( 'ssm' )

