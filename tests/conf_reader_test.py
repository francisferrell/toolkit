# mypy: allow-untyped-defs

from os import environ

import boto3
from moto import mock_aws
import pytest

from toolkit import conf



@pytest.fixture
def reader():
    return conf.Reader()



@pytest.fixture
def ssm():
    environ['AWS_ACCESS_KEY_ID'] = 'testing'
    environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
    environ['AWS_SECURITY_TOKEN'] = 'testing'
    environ['AWS_SESSION_TOKEN'] = 'testing'
    environ['AWS_DEFAULT_REGION'] = 'us-east-1'

    with mock_aws():
        yield boto3.client( 'ssm' )



def test_set_and_get( reader ):
    reader.set( 'foo', 42 )
    assert reader.get( 'foo' ) is 42



def test_contains( reader ):
    assert not 'foo' in reader
    reader.set( 'foo', 42 )
    assert 'foo' in reader



def test_get_raises_missing( reader ):
    assert 'foo' not in reader
    with pytest.raises( conf.Missing ):
        reader.get( 'foo' )



def test_get_default( reader ):
    assert 'foo' not in reader
    assert reader.get( 'foo', default = 42 ) == 42
    assert reader.get( 'foo', 'yes' ) == 'yes'



def test_set_defaults( reader ):
    reader.set_defaults( {
        'foo': 42
    } )
    assert 'foo' in reader
    assert reader.get( 'foo' ) == 42



def test_set_defaults_no_clobber( reader ):
    reader.set( 'foo', True )
    reader.set_defaults( dict(
        foo = 42
    ) )
    assert reader.get( 'foo' ) is True



def test_parse_env( monkeypatch, reader ):
    monkeypatch.setenv( 'CONF_FOO', '42' )
    monkeypatch.setenv( 'CONF_BAR_BAZ', 'qux' )
    monkeypatch.setenv( 'CONF_NESTED__CONFIG_VALUE', 'true' )
    reader.parse_env()
    assert reader.get( 'foo' ) == '42'
    assert reader.get( 'bar_baz' ) == 'qux'
    assert reader.get( 'nested.config_value' ) == 'true'



def test_parse_aws_parameters( ssm, reader ):
    ssm.put_parameter( Name = '/App/Foo', Value = '42', Type = 'String' )
    ssm.put_parameter( Name = '/App/Bar_Baz', Value = 'qux', Type = 'String' )
    ssm.put_parameter( Name = '/App/nested/config-value', Value = 'true', Type = 'String' )

    reader.parse_aws_parameters( prefix = '/App' )

    assert reader.get( 'foo' ) == '42'
    assert reader.get( 'bar_baz' ) == 'qux'
    assert reader.get( 'nested.config_value' ) == 'true'



def test_parse_aws_parameters_root( ssm, reader ):
    ssm.put_parameter( Name = '/Foo', Value = '42', Type = 'String' )
    ssm.put_parameter( Name = '/Bar/Baz', Value = 'qux', Type = 'String' )

    reader.parse_aws_parameters( prefix = '/' )

    assert reader.get( 'foo' ) == '42'
    assert reader.get( 'bar.baz' ) == 'qux'



def test_parse_aws_parameters_pagination( ssm, reader ):
    for x in range( 100 ):
        ssm.put_parameter( Name = f'/app/foo{x}', Value = '42', Type = 'String' )

    reader.parse_aws_parameters( prefix = '/app' )

    for x in range( 100 ):
        assert f'foo{x}' in reader



def test_parse_aws_parameters_trailing_slash( ssm ):
    ssm.put_parameter( Name = '/App/Foo', Value = '42', Type = 'String' )
    ssm.put_parameter( Name = '/App/Bar_Baz', Value = 'qux', Type = 'String' )
    ssm.put_parameter( Name = '/App/nested/config_value', Value = 'true', Type = 'String' )

    with_slash = conf.Reader()
    with_slash.parse_aws_parameters( prefix = '/App/' )
    without_slash = conf.Reader()
    without_slash.parse_aws_parameters( prefix = '/App' )

    assert with_slash.get( 'foo' ) == without_slash.get( 'foo' )
    assert with_slash.get( 'bar_baz' ) == without_slash.get( 'bar_baz' )
    assert with_slash.get( 'nested.config_value' ) == without_slash.get( 'nested.config_value' )



def test_order_parse_env_aws_parameters( monkeypatch, ssm, reader ):
    monkeypatch.setenv( 'CONF_FOO', 'from ENV' )
    monkeypatch.setenv( 'CONF_BAR', 'from ENV' )
    ssm.put_parameter( Name = '/app/bar', Value = 'from AWS', Type = 'String' )
    ssm.put_parameter( Name = '/app/baz', Value = 'from AWS', Type = 'String' )

    reader.parse_env()
    reader.parse_aws_parameters( prefix = '/app' )

    assert reader.get( 'foo' ) == 'from ENV'
    assert reader.get( 'bar' ) == 'from AWS'
    assert reader.get( 'baz' ) == 'from AWS'



def test_order_parse_aws_parameters_env( monkeypatch, ssm, reader ):
    monkeypatch.setenv( 'CONF_FOO', 'from ENV' )
    monkeypatch.setenv( 'CONF_BAR', 'from ENV' )
    ssm.put_parameter( Name = '/app/bar', Value = 'from AWS', Type = 'String' )
    ssm.put_parameter( Name = '/app/baz', Value = 'from AWS', Type = 'String' )

    reader.parse_aws_parameters( prefix = '/app' )
    reader.parse_env()

    assert reader.get( 'foo' ) == 'from ENV'
    assert reader.get( 'bar' ) == 'from ENV'
    assert reader.get( 'baz' ) == 'from AWS'



def test_get_default_reader():
    reader1 = conf.get_default_reader()
    reader2 = conf.get_default_reader()
    assert reader1 is reader2

    reader1.set( 'foo', '1' )
    assert reader1.get( 'foo' ) == reader2.get( 'foo' )

