# mypy: allow-untyped-defs

import logging as stdlib_logging
from os import environ
import pytest

from toolkit import logging as toolkit_logging

AllValidLevels = [
    ( 'debug', stdlib_logging.DEBUG ),
    ( 'DEBUG', stdlib_logging.DEBUG ),
    ( 'info', stdlib_logging.INFO ),
    ( 'INFO', stdlib_logging.INFO ),
    ( 'warn', stdlib_logging.WARN ),
    ( 'WARN', stdlib_logging.WARN ),
    ( 'warning', stdlib_logging.WARNING ),
    ( 'WARNING', stdlib_logging.WARNING ),
    ( 'error', stdlib_logging.ERROR ),
    ( 'ERROR', stdlib_logging.ERROR ),
    ( 'critical', stdlib_logging.CRITICAL ),
    ( 'CRITICAL', stdlib_logging.CRITICAL ),
    ( 'fatal', stdlib_logging.FATAL ),
    ( 'FATAL', stdlib_logging.FATAL ),
]



def unset_all_child_loggers( logger: stdlib_logging.Logger ) -> None:
    for child in logger.getChildren():
        child.setLevel( stdlib_logging.NOTSET )
        unset_all_child_loggers( child )



@pytest.fixture( autouse = True )
def clean_environment( monkeypatch ):
    for k in environ:
        if k == 'LOG' or k.startswith( 'LOG_' ):
            monkeypatch.delenv( k )

    root = stdlib_logging.getLogger()
    root.setLevel( stdlib_logging.WARNING )
    unset_all_child_loggers( root )



def test_passes_through_stdlib():
    for k in stdlib_logging.__all__:
        x = getattr( stdlib_logging, k )
        toolkit_x = getattr( toolkit_logging, k )
        assert toolkit_x is x



@pytest.mark.parametrize( 'getLogger', [ toolkit_logging.getLogger, stdlib_logging.getLogger ] )
def test_no_config( getLogger ):
    toolkit_logging.configure_levels_from_env()

    assert getLogger().getEffectiveLevel() == stdlib_logging.WARNING
    assert getLogger().level == stdlib_logging.WARNING
    assert getLogger( 'foo' ).getEffectiveLevel() == stdlib_logging.WARNING
    assert getLogger( 'foo' ).level == stdlib_logging.NOTSET
    assert getLogger( 'foo.bar_baz' ).getEffectiveLevel() == stdlib_logging.WARNING
    assert getLogger( 'foo.bar_baz' ).level == stdlib_logging.NOTSET



@pytest.mark.parametrize( 'getLogger', [ toolkit_logging.getLogger, stdlib_logging.getLogger ] )
@pytest.mark.parametrize( 'name, expected_level', AllValidLevels )
def test_root_log_level( monkeypatch, name, expected_level, getLogger ):
    monkeypatch.setenv( 'LOG', name )

    toolkit_logging.configure_levels_from_env()

    assert getLogger().getEffectiveLevel() == expected_level
    assert getLogger().level == expected_level
    assert getLogger( 'foo' ).getEffectiveLevel() == expected_level
    assert getLogger( 'foo' ).level == stdlib_logging.NOTSET
    assert getLogger( 'foo.bar_baz' ).getEffectiveLevel() == expected_level
    assert getLogger( 'foo.bar_baz' ).level == stdlib_logging.NOTSET



@pytest.mark.parametrize( 'getLogger', [ toolkit_logging.getLogger, stdlib_logging.getLogger ] )
@pytest.mark.parametrize( 'name, expected_level', AllValidLevels )
def test_child_log_level( monkeypatch, name, expected_level, getLogger ):
    monkeypatch.setenv( 'LOG_FOO', name )

    toolkit_logging.configure_levels_from_env()

    assert getLogger().getEffectiveLevel() == stdlib_logging.WARNING
    assert getLogger().level == stdlib_logging.WARNING
    assert getLogger( 'foo' ).getEffectiveLevel() == expected_level
    assert getLogger( 'foo' ).level == expected_level
    assert getLogger( 'foo.bar_baz' ).getEffectiveLevel() == expected_level
    assert getLogger( 'foo.bar_baz' ).level == stdlib_logging.NOTSET



@pytest.mark.parametrize( 'getLogger', [ toolkit_logging.getLogger, stdlib_logging.getLogger ] )
@pytest.mark.parametrize( 'name, expected_level', AllValidLevels )
def test_log_level_after_creation( monkeypatch, name, expected_level, getLogger ):
    monkeypatch.setenv( 'LOG_FOO', name )
    root = getLogger()
    foo = getLogger( 'foo' )
    foo_bar = getLogger( 'foo.bar_baz' )

    toolkit_logging.configure_levels_from_env()

    assert root.getEffectiveLevel() == stdlib_logging.WARNING
    assert root.level == stdlib_logging.WARNING
    assert foo.getEffectiveLevel() == expected_level
    assert foo.level == expected_level
    assert foo_bar.getEffectiveLevel() == expected_level
    assert foo_bar.level == stdlib_logging.NOTSET



@pytest.mark.parametrize( 'getLogger', [ toolkit_logging.getLogger, stdlib_logging.getLogger ] )
@pytest.mark.parametrize( 'name, expected_level', AllValidLevels )
def test_grandchild_log_level( monkeypatch, name, expected_level, getLogger ):
    monkeypatch.setenv( 'LOG_FOO__BAR_BAZ', name )

    toolkit_logging.configure_levels_from_env()

    assert getLogger().getEffectiveLevel() == stdlib_logging.WARNING
    assert getLogger().level == stdlib_logging.WARNING
    assert getLogger( 'foo' ).getEffectiveLevel() == stdlib_logging.WARNING
    assert getLogger( 'foo' ).level == stdlib_logging.NOTSET
    assert getLogger( 'foo.bar_baz' ).getEffectiveLevel() == expected_level
    assert getLogger( 'foo.bar_baz' ).level == expected_level



@pytest.mark.parametrize( 'getLogger', [ toolkit_logging.getLogger, stdlib_logging.getLogger ] )
def test_all_log_level( monkeypatch, getLogger ):
    monkeypatch.setenv( 'LOG', 'debug' )
    monkeypatch.setenv( 'LOG_FOO', 'info' )
    monkeypatch.setenv( 'LOG_FOO__BAR_BAZ', 'debug' )

    toolkit_logging.configure_levels_from_env()

    assert getLogger().getEffectiveLevel() == stdlib_logging.DEBUG
    assert getLogger().level == stdlib_logging.DEBUG
    assert getLogger( 'foo' ).getEffectiveLevel() == stdlib_logging.INFO
    assert getLogger( 'foo' ).level == stdlib_logging.INFO
    assert getLogger( 'foo.bar_baz' ).getEffectiveLevel() == stdlib_logging.DEBUG
    assert getLogger( 'foo.bar_baz' ).level == stdlib_logging.DEBUG



@pytest.mark.parametrize( 'getLogger', [ toolkit_logging.getLogger, stdlib_logging.getLogger ] )
def test_ignore_invalid_log_level( monkeypatch, getLogger ):
    monkeypatch.setenv( 'LOG', 'debug' )
    monkeypatch.setenv( 'LOG_FOO', 'inf' )
    monkeypatch.setenv( 'LOG_FOO__BAR_BAZ', 'debug' )

    toolkit_logging.configure_levels_from_env()

    assert getLogger().getEffectiveLevel() == stdlib_logging.DEBUG
    assert getLogger().level == stdlib_logging.DEBUG
    assert getLogger( 'foo' ).getEffectiveLevel() == stdlib_logging.DEBUG
    assert getLogger( 'foo' ).level == stdlib_logging.NOTSET
    assert getLogger( 'foo.bar_baz' ).getEffectiveLevel() == stdlib_logging.DEBUG
    assert getLogger( 'foo.bar_baz' ).level == stdlib_logging.DEBUG

