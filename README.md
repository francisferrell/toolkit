
[![latest release](https://gitlab.com/francisferrell/toolkit/-/badges/release.svg)](https://gitlab.com/francisferrell/toolkit/-/releases)
[![pipeline status](https://gitlab.com/francisferrell/toolkit/badges/main/pipeline.svg)](https://gitlab.com/francisferrell/toolkit/-/pipelines)
[![coverage report](https://gitlab.com/francisferrell/toolkit/badges/main/coverage.svg)](https://gitlab.com/francisferrell/toolkit/-/tree/main)

# Toolkit

Batteries included wasn't quite enough.

[Documentation](https://toolkit-francisferrell-d6c8d19c523fcbd39f2addf5463bde82c1d6ba4d.gitlab.io/)

