
default: test lint mypy

.PHONY: test
test: venv/bin/activate
	./venv/bin/pytest

.PHONY: lint
lint: venv/bin/activate
	./venv/bin/pylint src/

.PHONY: mypy
mypy: venv/bin/activate
	./venv/bin/mypy src/ tests/

.PHONY: shell
shell: venv/bin/activate
	./venv/bin/python3 -i -c 'from toolkit import conf, logging'

.PHONY: docs
docs: venv/bin/activate
	./venv/bin/pdoc -o pdoc/ toolkit

venv/bin/activate: pyproject.toml
	rm -rf venv
	python3 -m venv venv
	./venv/bin/pip install -e .[dev]
venv: venv/bin/activate

.PHONY: clean
clean:
	rm -rf \
		.coverage \
		.mypy_cache/ \
		.pytest_cache/ \
		dist/ \
		pdoc/ \
		reports/ \
		venv/ \
		;
	find . -type d -name __pycache__ -prune -print -exec rm -rf {} \;

